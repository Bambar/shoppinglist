import firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyBRx2n5e6VuwImzSJNguEU56i7O0ektN-A",
  authDomain: "shopping-list-89aad.firebaseapp.com",
  projectId: "shopping-list-89aad",
  storageBucket: "shopping-list-89aad.appspot.com",
  messagingSenderId: "926113850615",
  appId: "1:926113850615:web:66ea729ef1a16979d19ff2",
  measurementId: "G-CZBLP2H6JW"
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();

export { db };

