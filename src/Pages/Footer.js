import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from "@material-ui/core";


const useStyles = makeStyles((theme) => ({
  mainFooter:{
    color: 'white',
    backgroundColor: '#808081',
    paddingTop: '3em',
    position: 'fixed',
    bottom: '0',
    width: '100%',
    height:'15vh',
  },
  add: {
    paddingLeft:'47vw',
    textDecoration: 'none',
    color:'whitesmoke',
    flexGrow:0.2,
    fontFamily:'Roboto, Helvetica, Arial, sans-serif',
    fontSize:'20px',
  },
  search: {
    paddingLeft:'10vw',
    textDecoration: 'none',
    color:'whitesmoke',
    flexGrow:0.2,
    fontFamily:'Roboto, Helvetica, Arial, sans-serif',
    fontSize:'20px',
  },
  report: {
    paddingLeft:'10vw',
    textDecoration: 'none',
    color:'whitesmoke',
    flexGrow: 0.2,
    fontFamily:'Roboto, Helvetica, Arial, sans-serif',
    fontSize:'20px',
  },
  list: {
    alignItems: 'space-evenly',
    listStyleType: 'none',
  },
  title:{
    paddingLeft:'7vw',
    fontFamily:'Roboto, Helvetica, Arial, sans-serif',
    fontSize:'20px',
    flexGrow:1,
  },
}));


function Footer() {
  const classes = useStyles();

  return (
    <div className={classes.mainFooter}>
      <div>
        <div>
          <table>
            <tr>
              <th>
                <Typography variant="h6" className={classes.title}>
                  IShop
                </Typography>
              </th>
              <th>
                <ui className={classes.list}>
                  <a className={classes.add} href="/add">Add</a>
                  <a className={classes.search} href="/search-products">Search</a>        
                  <a className={classes.report} href="/reports">Report</a>      
                </ui>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  );
}

export default Footer;