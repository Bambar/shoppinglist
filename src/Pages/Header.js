import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import SLogo from '../assets/images/SLogo.png'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  add: {
    textDecoration: 'none',
    color:'whitesmoke',
    flexGrow:0.2,
    fontFamily:'Roboto, Helvetica, Arial, sans-serif',
    fontSize:'20px',
  },
  search: {
    textDecoration: 'none',
    color:'whitesmoke',
    flexGrow:0.2,
    fontFamily:'Roboto, Helvetica, Arial, sans-serif',
    fontSize:'20px',
  },
  report: {
    textDecoration: 'none',
    color:'whitesmoke',
    flexGrow:0.2,
    fontFamily:'Roboto, Helvetica, Arial, sans-serif',
    fontSize:'20px',
  },
}));

export default function Header() {
  const classes = useStyles();

  return (
    <div style={{flexGrow:1}}>
      <AppBar position="static"style={{backgroundColor:'#53B175'}}>
        <Toolbar>
          <img src={SLogo} alt="SLogo" style={{paddingRight:30,width:50,}}/>
          <Typography variant="h6" style={{flexGrow:1,}}>
            IShop
          </Typography>
          <a className={classes.add} href="/add">Add</a>
          <a className={classes.search} href="/search-products">Search</a>
          <a className={classes.report} href="/reports">Report</a>
        </Toolbar>
      </AppBar>
    </div>
  );
}
