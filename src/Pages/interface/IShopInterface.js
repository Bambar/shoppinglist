import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import SLogo from '../../assets/images/SLogo.png';

const useStyles = makeStyles((theme) => ({
  app: {
    textAlign: 'center',
  },
  applogo:{
    height: '40vmin',
    marginTop: '20vh',
    pointerEvents: 'none',
  },
  appheader:{
    backgroundColor: '#53B175',
    height: '100vh',
    justifyContent: 'center',
    fontSize: '20px',
    fontFamily: 'Poppins, sans-serif',
    fontWeight: '400',    
    color:'white',
  },
  applink: {
    color: '#61dafb',
  },
}));

export default function IShopInterface() {
  const classes = useStyles();

  return (
    <div className={classes.app}>
      <header className={classes.appheader}>
        <img src={SLogo} className={classes.applogo} alt="SLogo"/>
        <p>Welcome to IShop</p>
        <a className={classes.applink} href="/add">
          Get Started
        </a>
      </header>
    </div>
  )
}
