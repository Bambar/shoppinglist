import React from 'react'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import IShopInterface from './interface/IShopInterface';
import IShopAdd from './add/IShopAdd';
import SearchProducts from './search/SearchProducts';
import ProductReports from './report/ProductReports';

export default function IShop() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact={true}>
          <IShopInterface/>
        </Route>
        <Route path="/add" exact={true}>
          <IShopAdd/>
        </Route>
        <Route path="/search-products" exact={true}>
          <SearchProducts/>
        </Route>
        <Route path="/reports" exact={true}>
          <ProductReports/>
        </Route>
      </Switch>
    </Router>
  )
}


