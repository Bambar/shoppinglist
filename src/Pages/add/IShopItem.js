import React from 'react';
import { ListItem, ListItemText, Button } from '@material-ui/core';
import { db } from '../../firebase_config';

export default function IShopItem({ name, id }) {

  function deleteIShop() {
    db.collection("Items").doc(id).delete();
  }

  return (
    <div style={{ display: "flex" }}>
      <ListItem>
        <ListItemText primary={name}/>
      </ListItem>
      <Button onClick={deleteIShop}>X</Button>
    </div>
  );
}