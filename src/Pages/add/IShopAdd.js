import React from 'react';
import firebase from 'firebase';
import Header from '../Header';
import Footer from '../Footer';
import { db } from '../../firebase_config';
import TextField from '@material-ui/core/TextField';
import { useState, useEffect } from 'react';
import { Button } from '@material-ui/core';
import IShopItem from './IShopItem';

export default function IShopAdd() {
  const [shops, setShops] = useState([]);
  const [shopInput, setShopInput] = useState("");

  useEffect(() => {
    getShops();
  }, []); 

  function getShops() {
    db.collection("Items").onSnapshot(function (querySnapshot) {
      setShops(
        querySnapshot.docs.map((doc) => ({
          id: doc.id,
          date: doc.date,
          name: doc.data().name,
        }))
      );
    });
  }

  function addShop(e) {
    e.preventDefault();

    db.collection("Items").add({
      name: shopInput,
      date: firebase.firestore.FieldValue.serverTimestamp(),
      
    });

    setShopInput("");
  }

  return (
    <div className="App">
      <Header/>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          width: "100%",
        }}
      >
        
        <form>
          <TextField
            id="standard-basic"
            label="Write a Shopping list"
            value={shopInput}
            style={{ width: "90vw", maxWidth: "500px" }}
            onChange={(e) => setShopInput(e.target.value)}
          />
          <Button
            type="submit"
            variant="contained"
            onClick={addShop}
            style={{ display: "none" }}
          >
            Default
          </Button>
        </form>

        <div style={{ width: "90vw", maxWidth: "500px", marginTop: "24px" }}>
          {shops.map((shop) => (
            <IShopItem
              name={shop.name}
              id={shop.id}
              date={shop.date}
            />
          ))}
        </div>
      </div>
      <Footer/>
    </div>
  );
}